from distutils.core import setup

setup(
    name='SkewT',
    version='1.2.0',
    author='Thomas Chubb',
    author_email='thomas.chubb@gmail.com',
    packages=['skewt'],
    scripts=[],
    url='http://pypi.python.org/pypi/SkewT/',
    license='LICENSE.txt',
    description='Plots and analyses atmospheric profile data from UWyo database',
    long_description=open('README.rst').read(),
)
